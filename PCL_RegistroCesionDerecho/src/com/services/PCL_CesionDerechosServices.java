package com.services;

import com.testCase.TestParameters;

public class PCL_CesionDerechosServices extends HostServices{
	public PCL_CesionDerechosServices() {
		super();
	}
	public void validarGarantia(TestParameters caso) throws Exception {
		this.waitForInputReady();
		this.sendKeysOn(20, 44, "pcl");
		this.sendEnter();
		this.waitForInputReady();
		this.sendKeysOn(13, 40, "01");
		this.sendKeys(caso.getCodigoUnico());
		this.sendEnter();
		this.waitForInputReady();
		this.sendPF(8);
		this.waitForText(12, 43, 11, "Otras Garan");
		this.waitForInputReady();
		this.setCursorPos(12, 43);
		this.sendEnter();
		buscarGarantia();
		buscarNumeroGarantia(caso);
	}
	public void buscarGarantia() throws Exception {
		for (int row=8;row<=18;row++) {
			String codigoDocumento = this.getText(row, 9, 30);
			System.out.println(codigoDocumento);
			if(codigoDocumento.contains("CESION DERECHO EN GARANTIA")) {
				this.sendKeysOn(row, 2, "X");
				this.sendEnter();
				this.waitForInputReady();
				return;
			}
		}
		throw new Exception("No se encontro el menu: CESION DERECHO EN GARANTIA");
	}
	public void buscarNumeroGarantia(TestParameters caso) throws Exception {
		String mensaje = this.getText(22, 3, 17);
		while(mensaje.equals("FIN DE CONSULTA...")) {
			for (int row=9;row<=19;row++) {
				String codigoDocumento = this.getText(row, 4, 10);
				if(codigoDocumento.contains(caso.getNumGarantia())) {
					this.sendKeysOn(row, 2, "X");
					this.sendEnter();
					return;
				}
			}
			this.sendPF(8);
			mensaje = this.getText(22, 3, 17);
		}
		throw new Exception("No se encontro el numero de garantia " + caso.getNumGarantia());
	}
}
