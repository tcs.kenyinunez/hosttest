package com.services;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.InputStream;
import java.util.Properties;

import com.jacob.activeX.ActiveXComponent;
import com.jacob.com.Dispatch;
import com.jacob.com.LibraryLoader;
import com.jacob.com.Variant;

public class HostServices extends SharedServices{
	private ActiveXComponent ps;
	private ActiveXComponent op;
	private int timeOut = 20000;
	private Properties properties = new Properties();
	
	public HostServices() {
		
	      try {
	    	  properties.load(new FileReader("config.properties"));
	 		  
			  String ruta64 = properties.getProperty("rutajacob64");
			  String ruta32 = properties.getProperty("rutajacob32");
			  String libFile = System.getProperty("os.arch").equals("amd64") ? ruta64 : ruta32;
			  
			  
	          /* Read DLL file*/
	          InputStream inputStream = new FileInputStream(libFile);
	          /**
	           *  Step 1: Create temporary file under <%user.home%>\AppData\Local\Temp\jacob.dll 
	           *  Step 2: Write contents of `inputStream` to that temporary file.
	           */
	          File temporaryDll = File.createTempFile("jacob", ".dll");
	          FileOutputStream outputStream = new FileOutputStream(temporaryDll);
	          byte[] array = new byte[8192];
	          for (int i = inputStream.read(array); i != -1; i = inputStream.read(array)) {
	              outputStream.write(array, 0, i);
	          }
	          outputStream.close();
	          /**
	           * `System.setProperty(LibraryLoader.JACOB_DLL_PATH, temporaryDll.getAbsolutePath());`
	           * Set System property same like setting java home path in system.
	           * 
	           * `LibraryLoader.loadJacobLibrary();`
	           * Load JACOB library in current System.
	           */
	          System.setProperty(LibraryLoader.JACOB_DLL_PATH, temporaryDll.getAbsolutePath());
	          LibraryLoader.loadJacobLibrary();

	          /**
	           * Create ActiveXComponent using CLSID. You can also use program id here.
	           * Next line(commented line/compProgramID) shows you how you can create ActiveXComponent using ProgramID.
	           */
	          ps = new ActiveXComponent("PCOMM.autECLPS");
	          op = new ActiveXComponent("PCOMM.autECLOIA");
	          
	          Dispatch.call(op,"SetConnectionByName",new Variant("A"));
	          Dispatch.call(ps,"SetConnectionByName",new Variant("A"));         
	          System.out.println("The Library been loaded, and an activeX component been created");
	          
	          temporaryDll.deleteOnExit();
	      } catch (Exception e) {
	          e.printStackTrace();
	      }
	}
	public void sendKeys(String keys) {
		Dispatch.call(ps,"SendKeys",new Variant(keys));
		this.waitForInputReady();
	}
	public String getText(int AxisX, int AxisY, int length) {
		Variant mensaje = Dispatch.call(ps,"GetText",new Variant(AxisX), new Variant(AxisY), new Variant(length));
		this.waitForInputReady();
		return mensaje.toString();
	}
	public void sendEnter() {
		Dispatch.call(ps,"SendKeys",new Variant("[ENTER]"));
		this.waitForInputReady();
	}
	public void sendTAB() {
		Dispatch.call(ps,"SendKeys",new Variant("[TAB]"));
		this.waitForInputReady();
	}
	public void sendClear() {
		Dispatch.call(ps,"SendKeys",new Variant("[clear]"));
		this.waitForInputReady();
	}
	public void sendPF(int number) {
		System.out.println("[pf" + number + "]");
		Dispatch.call(ps,"SendKeys",new Variant("[pf" + number + "]"));
		this.waitForInputReady();
	}
	public void waitForInputReady() {
		Dispatch.call(op,"WaitForInputReady", new Variant(this.timeOut));
	}
	public void setCursorPos(int AxisX, int AxisY) {
		Dispatch.call(ps, "SetCursorPos", new Variant(AxisX), new Variant(AxisY));
		this.waitForInputReady();
	}
	public void waitForCursor(int AxisX,int AxisY) {
		Dispatch.call(ps, "WaitForCursor", new Variant(AxisX), new Variant(AxisY), new Variant(this.timeOut));
		this.waitForInputReady();
	}
	public void sendKeysOn(int AxisX,int AxisY, String cadena) {
		this.setCursorPos(AxisX, AxisY);
		this.waitForInputReady();
		this.sendKeys(cadena);
		this.waitForInputReady();
	}
	public void waitForText(int AxisX, int AxisY, int length, String text) throws Exception {
		for(int wait=0;wait<60;wait++) {
			System.out.println(this.getText(AxisX, AxisY, length));
			if ((this.getText(AxisX, AxisY, length).contains(text))) {
				return;
			}
			Thread.sleep(500);
		}
		throw new Exception("No se encontro el texto  " + text);
	}
}
