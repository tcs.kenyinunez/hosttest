package com.services;

import java.io.FileNotFoundException;
import java.io.IOException;

public class LoginHostServices extends HostServices{
	public LoginHostServices() {
		super();
	}
	public void login(String user, String pass) throws Exception {
		this.focusWindow("Sesi�n B - [24 x 80]");
		this.waitForInputReady();
		this.waitForCursor(6, 1);
		this.sendKeys("logon applid(access30)");
		this.sendEnter();
		this.waitForInputReady();
		this.waitForText(2, 2, 6, "ACCESS");
		Thread.sleep(1000);
		this.waitForInputReady();
		this.sendKeysOn(10, 68, user);
		this.sendKeysOn(11,68,pass);
		this.waitForInputReady();
		this.sendEnter();
		this.waitForText(1, 2, 6, "EMSP01");
		this.sendKeys("CICSAA2K");
		this.sendEnter();
		this.waitForInputReady();
		this.waitForText(20, 59, 9, "CONSULTA");
		this.sendKeys("ib00");
		this.sendEnter();
		this.waitForInputReady();
		String mensaje = this.getText(1, 2, 4);
		if(mensaje.contains("CICS")) {
			this.waitForInputReady();
			this.waitForCursor(10, 71);
			this.sendKeysOn(10, 71, user);
			this.sendKeysOn(11, 71, pass);
			this.sendEnter();
		}
	}
	public void logOffMenu() throws InterruptedException {
		this.waitForInputReady();
		String titulo = this.getText(1, 28, 25);
		while(!titulo.contains("SISTEMAS DE INFORMACION")) {
			System.out.println(titulo);
			this.sendPF(3);
			Thread.sleep(500);
			titulo = this.getText(1, 28, 25);
		}
	}
	public void logOff() {
		try {
			logOffMenu();
			System.out.println("Entro a logoff");
			this.waitForInputReady();
			this.sendPF(3);
			this.waitForText(1, 2, 5, "CICS");
			this.waitForInputReady();
			this.sendClear();
			this.sendPF(3);
			this.waitForText(1, 24, 25, "Seleccion de Aplicaciones");
			this.sendPF(12);
			this.waitForText(1, 19, 5, "LOGON");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
