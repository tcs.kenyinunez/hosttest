package com.utilites;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.Properties;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.FormulaEvaluator;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import com.testCase.TestParameters;


public class Excel {
	public static FileInputStream fi;
	public static FileOutputStream fo;
	public static XSSFWorkbook wb;
	public static XSSFSheet ws;
	public static XSSFRow row;
	public static XSSFCell cell;
	private Properties properties = new Properties();
	public static String nombreArchivo;
	public Excel() {
		try {
			properties.load(new FileReader("config.properties"));
			this.nombreArchivo = properties.getProperty("ruta");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	public String obtenerValorCelda(int indRow,int indCell) throws IOException {
		fi=new FileInputStream(nombreArchivo);
		wb=new XSSFWorkbook(fi);
		ws=wb.getSheet("Entrada");
		row=ws.getRow(indRow);
		cell=row.getCell(indCell);
		String data;
		try 
		{
			DataFormatter formatter = new DataFormatter();
            String cellData = formatter.formatCellValue(cell);
            return cellData;
		}
		catch (Exception e) 
		{
			data="";
		}
		wb.close();
		fi.close();
		return data;
	}
	
	public void establecerValorCelda(int indRow, int indCell, String data) {
		try {
			fi=new FileInputStream(nombreArchivo);
			wb=new XSSFWorkbook(fi);
			ws=wb.getSheet("Entrada");
			row=ws.getRow(indRow);
			cell=row.createCell(indCell);
			cell.setCellValue(data);
			fo=new FileOutputStream(nombreArchivo);
			wb.write(fo);		
			wb.close();
			fi.close();
			fo.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	public void actualizarExcel(ArrayList<TestParameters> arrayTest) {
		String hoja = "Entrada";
		try(FileInputStream file = new FileInputStream(new File(nombreArchivo))){
			wb = new XSSFWorkbook(file);
			ws = wb.getSheetAt(0);
			for(int indRow=1;indRow<=getRowCount()-1;indRow++) {
				row = ws.getRow(indRow);
				row.getCell(0).setCellValue(arrayTest.get(indRow).getCaso());
				row.getCell(1).setCellValue(arrayTest.get(indRow).getMensaje());	
				row.getCell(2).setCellValue(arrayTest.get(indRow).getFlag());
				row.getCell(3).setCellValue(arrayTest.get(indRow).getCodigoUnico());
				row.getCell(4).setCellValue(arrayTest.get(indRow).getNumGarantia());
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
	}
	public void generarExcel() {
		String hoja = "Entrada";
		XSSFWorkbook libro = new XSSFWorkbook();
		XSSFSheet hoja1 = libro.createSheet(hoja);
		
		String[] header = new String[] 
				{"caso","outMensaje","inFlag","inNumGarantia"};
		XSSFRow row = hoja1.createRow(0);
		for(int i=0; i < header.length;i++) {
			XSSFCell cell = row.createCell(i);
			cell.setCellValue(header[i]);
		}
		try (OutputStream fileOut = new FileOutputStream("PCL_FianzaSolidaria.xlsx")){
			System.out.println("Se creo el excel");
			libro.write(fileOut);
		}catch(IOException e) {
			e.printStackTrace();
		}
	}
	public int getRowCount() throws IOException {
		fi = new FileInputStream(nombreArchivo);
		wb = new XSSFWorkbook(fi);
		ws = wb.getSheet("Entrada");
		int rowCount = ws.getLastRowNum();
		return rowCount;
	}

	public ArrayList<TestParameters> leerExcel() throws IOException, ParseException {
		fi = new FileInputStream(nombreArchivo);
		wb = new XSSFWorkbook(fi);
		ws = wb.getSheet("Entrada");
		int rowCount = ws.getLastRowNum();
		ArrayList<TestParameters> arrayObject = new ArrayList<TestParameters>();
		for(int indexRow=1;indexRow<=rowCount;indexRow++) {
			row = ws.getRow(indexRow);
			TestParameters tcParameters = new TestParameters();
			DataFormatter formatter = new DataFormatter();
			tcParameters.setCaso(formatter.formatCellValue(row.getCell(0)));
			tcParameters.setMensaje(formatter.formatCellValue(row.getCell(1)));
			tcParameters.setFlag(formatter.formatCellValue(row.getCell(2)));
			tcParameters.setCodigoUnico(formatter.formatCellValue(row.getCell(3)));
			tcParameters.setNumGarantia(formatter.formatCellValue(row.getCell(4)));
			arrayObject.add(tcParameters);
		}
		wb.close();
		fi.close();
		return arrayObject;
	}
	public String getFormatDate(String cadena, String date) throws ParseException {
		SimpleDateFormat  parseador = new SimpleDateFormat(cadena);
		DecimalFormat parseadorDecimal = new DecimalFormat("##");
		Date diaConvertido = parseador.parse(date);
		String strfecha = "";
		Calendar cal = Calendar.getInstance();
		cal.setTime(diaConvertido);
		String strDia = parseadorDecimal.format(cal.get(Calendar.DAY_OF_MONTH));
		String strMes = parseadorDecimal.format(cal.get(Calendar.MONTH) +1);
		strfecha = strDia + "/" + strMes + "/" + cal.get(Calendar.YEAR);
		return strfecha;
	}
}
