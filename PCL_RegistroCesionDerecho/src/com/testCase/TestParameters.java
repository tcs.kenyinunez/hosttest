package com.testCase;

public class TestParameters {
	private String caso;
	private String flag;
	private String mensaje;
	private String codigoUnico;
	private String numGarantia;
	
	public String getCodigoUnico() {
		return codigoUnico;
	}
	public void setCodigoUnico(String codigoUnico) {
		this.codigoUnico = codigoUnico;
	}
	public String getCaso() {
		return caso;
	}
	public void setCaso(String caso) {
		this.caso = caso;
	}
	public String getFlag() {
		return flag;
	}
	public void setFlag(String flag) {
		this.flag = flag;
	}
	public String getMensaje() {
		return mensaje;
	}
	public void setMensaje(String mensaje) {
		this.mensaje = mensaje;
	}
	public String getNumGarantia() {
		return numGarantia;
	}
	public void setNumGarantia(String numGarantia) {
		this.numGarantia = numGarantia;
	}
	
}	
