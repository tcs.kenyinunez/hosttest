package com.testCase;

import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;
import com.services.HostServices;
import com.services.LoginHostServices;
import com.services.PCL_CesionDerechosServices;
import com.services.SharedServices;
import com.sun.jna.platform.win32.Wdm;
import com.utilites.Excel;
import com.utilites.ExtendReportsUtilities;

import org.testng.annotations.DataProvider;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;

import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Properties;

import org.testng.SkipException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;

public class RegistroCesionDerecho_PCL {
	private ExtendReportsUtilities report = new ExtendReportsUtilities();
	private LoginHostServices services = new LoginHostServices();
	private PCL_CesionDerechosServices fianzaServices = new PCL_CesionDerechosServices();
	private Properties properties = new Properties();
	private SharedServices sharedServices = new SharedServices();
	private String rutaReporte = this.sharedServices.getDirectorioProyecto() + "\\reportes\\" + this.sharedServices.getHoraExacta();
	private Excel excel = new Excel();
	
	@BeforeClass
	  public void beforeClass() {
		  report.setUp(rutaReporte);
		  report.test();
		  report.crearDirectivo();
	  }
	@BeforeMethod
	public void beforeMethod() {
		
	}
	
	@DataProvider(name ="dp")
	  public Object[][] dp() throws ParseException {
	    Object[][] arrayObject = null;
	    ArrayList<TestParameters> arrayExcel = null;
	    try {
	    	arrayExcel = excel.leerExcel();
	    	arrayObject = new Object[arrayExcel.size()][2];
	    	int i = 0;
	    	for(TestParameters caso: arrayExcel) {
	    		arrayObject[i][0] = String.valueOf(i);
	    		arrayObject[i][1] = caso;
	    		i++;
	    	}
	    }catch(IOException e) {
	    	e.printStackTrace();
	    }
	    return arrayObject;
	  }
	  @Test(dataProvider = "dp")
	  public void testMain(String index, TestParameters caso) {
		  System.out.println(caso.getMensaje());
		  if (caso.getMensaje().equalsIgnoreCase("exito")) {
			  try {
				  report.getTestObject().info("Inicio de Caso " + caso.getCaso());
				  properties.load(new FileReader("config.properties"));
				  this.services.login(properties.getProperty("user"), properties.getProperty("pass"));
				  this.fianzaServices.validarGarantia(caso);
				  report.getScreenShotWithState(Status.PASS,
						  "Caso :"  + caso.getCaso() + " Validacion de numero Garantia: " + caso.getNumGarantia(), 
						  rutaReporte + "\\screenshots\\" + this.sharedServices.getHoraExacta() + ".png", null);
				  this.services.logOff();
				  excel.establecerValorCelda(Integer.parseInt(caso.getCaso()), 2, "1");
				  excel.establecerValorCelda(Integer.parseInt(caso.getCaso()), 1, "Validado");
			  }catch(Exception e) {
				  report.getScreenShotWithState(Status.ERROR,
						  e.getMessage(), rutaReporte + "\\screenshots\\" + this.sharedServices.getHoraExacta() + ".png"
						  , null);
				  excel.establecerValorCelda(Integer.parseInt(caso.getCaso()), 2, "7");
				  excel.establecerValorCelda(Integer.parseInt(caso.getCaso()), 1, e.getMessage());
				  this.services.logOff();
				  e.printStackTrace();
			  }
			  report.getTestObject().info("Fin de Caso");
		  }else {
			  report.getTestObject().skip("Se salteo este caso : " + caso.getCaso());
			  throw new SkipException("Se ha salteado este caso");
		  }
	  }	
	  @AfterClass
	  public void afterClass() throws Throwable {
		  report.getTestObject().info("Automatizacion Completa");
		  report.terminarReporte(rutaReporte + "\\report.html");
	  }

}
